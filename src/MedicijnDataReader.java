import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MedicijnDataReader
{
    private final String xmlFile = "out/production/zorgApp/Database/MedicijnDataBase.XML";

    public ArrayList<Medicijn> getMedicijnList()
    {
        try
        {
            return parseXmlData(xmlFile);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
            return null;
        }
    }

    private ArrayList<Medicijn> parseXmlData (String xmlFile) throws Exception
    {
        ArrayList<Medicijn> medicijnList = new ArrayList<>();
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document myFile = documentBuilder.parse(new File(xmlFile));
        myFile.getDocumentElement().normalize();
        NodeList nodeList = myFile.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) node;

                String dataType = node.getAttributes().getNamedItem("type").getNodeValue();

                if (dataType.equals("Medicijn"))
                {
                    String medicijnnaam = element.getElementsByTagName("medicijnNaam")
                            .item(0)
                            .getChildNodes()
                            .item(0)
                            .getNodeValue();

                    String omschrijving = element.getElementsByTagName("Omschrijving")
                            .item(0)
                            .getChildNodes()
                            .item(0)
                            .getNodeValue();

                    String soort = element.getElementsByTagName("Soort")
                            .item(0)
                            .getChildNodes()
                            .item(0)
                            .getNodeValue();

                    String dosering = element.getElementsByTagName("Dosering")
                            .item(0)
                            .getChildNodes()
                            .item(0)
                            .getNodeValue();

                    Medicijn medicijn = new Medicijn(medicijnnaam, omschrijving, soort, dosering);

                    medicijnList.add(medicijn);
                }
            }

        }
        return  medicijnList;
    }
}
