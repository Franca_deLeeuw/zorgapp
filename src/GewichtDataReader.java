import java.lang.Integer;
import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GewichtDataReader
{
    private final String xmlFile = "out/production/zorgApp/Database/GewichtDataBase.XML";

    public ArrayList<GewichtMeetPunt> getGewichtList()
    {
        try
        {
            return parseXmlData(xmlFile);
        }
        catch (Exception e)
        {
            return null;
        }
    }
    private ArrayList<GewichtMeetPunt> parseXmlData(String xmlFile) throws Exception
    {
        ArrayList<GewichtMeetPunt> gewichtList = new ArrayList<>();
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document myFile = documentBuilder.parse(new File(xmlFile));
        myFile.getDocumentElement().normalize();
        NodeList nodeList = myFile.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) node;

                String dataType = node.getAttributes().getNamedItem("type").getNodeValue();

                if (dataType.equals("Gewicht"))
                {
                   double  gewicht = Double.parseDouble(element.getElementsByTagName("Gewicht")
                                    .item(0)
                                    .getChildNodes()
                                    .item(0)
                                    .getNodeValue());

                   String datum = element.getElementsByTagName("Datum")
                                    .item(0)
                                    .getChildNodes()
                                    .item(0)
                                    .getNodeValue();
                   String tijd = element.getElementsByTagName("Tijd")
                                    .item(0)
                                    .getChildNodes()
                                    .item(0)
                                    .getNodeValue();

                   GewichtMeetPunt gewichtMeetPunt = new GewichtMeetPunt(gewicht, datum, tijd);
                   gewichtList.add(gewichtMeetPunt);
                }
            }
        }
        return gewichtList;
    }
}
