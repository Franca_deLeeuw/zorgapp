import java.lang.Integer;
import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class ProfielDataReader
{
    private final String xmlFile = "out/production/zorgApp/Database/ProfielDataBase.XML";

    public ArrayList<Profile> getProfileList()
    {
         try
         {
              return parseXmlData(xmlFile);
         }
         catch (Exception e)
         {
             System.out.println(e.toString());
              return null;
         }
    }
private ArrayList<Profile> parseXmlData(String xmlFile) throws Exception
    {
         ArrayList<Profile> profileList = new ArrayList<>();
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document myFile = documentBuilder.parse(new File(xmlFile));
        myFile.getDocumentElement().normalize();
        NodeList nodeList = myFile.getDocumentElement().getChildNodes();


        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) node;

                String dataType = node.getAttributes().getNamedItem("type").getNodeValue();

                if (dataType.equals("Profiel"))
                {
                    String voorNaam = element.getElementsByTagName("Voornaam")
                                        .item(0)
                                        .getChildNodes()
                                        .item(0)
                                        .getNodeValue();
                    String achterNaam =  element.getElementsByTagName("Achternaam")
                                        .item(0)
                                        .getChildNodes()
                                        .item(0)
                                        .getNodeValue();
                    int leeftijd = Integer.parseInt(element.getElementsByTagName("Leeftijd")
                                        .item(0)
                                        .getChildNodes()
                                        .item(0)
                                        .getNodeValue());
                    double gewicht = Double.parseDouble(element.getElementsByTagName("Gewicht")
                                        .item(0)
                                        .getChildNodes()
                                        .item(0)
                                        .getNodeValue());
                    double lengte = Double.parseDouble(element.getElementsByTagName("Lengte")
                                        .item(0)
                                        .getChildNodes()
                                        .item(0)
                                        .getNodeValue());

                    Profile profile = new Profile(voorNaam, achterNaam, leeftijd, gewicht, lengte);
                    profileList.add(profile);
                }
            }
        }
    return profileList;
    }
}
