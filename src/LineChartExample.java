import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import java.util.*;
import javax.swing.*;
import java.util.ArrayList;

public class LineChartExample extends JFrame
{
    private static ResourceBundle bundle = ResourceBundle.getBundle("Messagebundle",
            new Locale("nl", "NL"));

    public ChartPanel LineChartExample(String title)
    {
        // Dataset maken
        DefaultCategoryDataset dataset = createDataset();

        // Grafiek maken
        JFreeChart grafiek = ChartFactory.createLineChart(
                bundle.getString("gewicht grafiek"),
                bundle.getString("datum"),
                bundle.getString("gewicht"),
                dataset
        );

        ChartPanel grafiekPanel = new ChartPanel(grafiek);
        setContentPane(grafiekPanel);

        return grafiekPanel;
    }


    private DefaultCategoryDataset createDataset()
    {
        String series1 = bundle.getString("gewicht");

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.addValue(20.5, series1, "1-1-2020 10:30");
        dataset.addValue(90.3, series1, "1-2-2020 15:00");
        dataset.addValue(79.8, series1, "1-3-2020 20:15");
        dataset.addValue(35.7, series1, "1-4-2020 13:15");


        return dataset;
    }

}
