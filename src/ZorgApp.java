import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.*;
import java.util.TimerTask;
import javax.swing.JFrame;




public class ZorgApp extends JFrame
{


    private static ArrayList<Profile> profielLijst = new ArrayList<>();
    private static ArrayList<Medicijn> medicijnLijst = new ArrayList<>();
    private static LineChartExample gewichtGrafiek = new LineChartExample();
    private static ProfielDataReader profielDataReader =  new ProfielDataReader();
    private static MedicijnDataReader medicijnDataReader = new MedicijnDataReader();
    private static Timer timer = new Timer();
    private static ResourceBundle bundle = ResourceBundle.getBundle("Messagebundle", new Locale("nl", "NL"));

    private void taalPopUp()
    {
        JFrame taalFrame = new JFrame();
        taalFrame.setBounds(500,400,400,250);
        JPanel taalPanel= new JPanel();
        taalPanel.setBounds(500,400,400,250);
        taalPanel.setLayout(null);
        JLabel taalTekstNL = new JLabel("Selecteer uw voorkeurs taal");
        taalTekstNL.setBounds(20,15,300,30);

        JLabel taalTekstEN = new JLabel("Please select your preferred language");
        taalTekstEN.setBounds(20,50,300,30);

        JLabel taalTekstFR = new JLabel("S'il vous plaît choisir votre langue préférée");
        taalTekstFR.setBounds(20,85,300,30);

        JButton NLknop = new JButton("Nederlands");
        NLknop.setBounds(20,130,110,25);

        JButton ENknop = new JButton("English");
        ENknop.setBounds(140,130,110,25);

        JButton FRknop = new JButton("Français");
        FRknop.setBounds(260,130,110,25);

        Locale NLLocale = new Locale("nl", "NL");
        Locale ENLocale = new Locale("en", "GB");
        Locale FRLocale = new Locale("fr", "FR");


        NLknop.addActionListener(e ->
                {
                    bundle = ResourceBundle.getBundle("Messagebundle", NLLocale);
                    showFrame();
                    taalFrame.dispose();
                }
                );
        ENknop.addActionListener(e ->
                 {
                     bundle = ResourceBundle.getBundle("Messagebundle", ENLocale);
                     showFrame();
                     taalFrame.dispose();
                 }
                 );

        FRknop.addActionListener(e ->
                {
                    bundle = ResourceBundle.getBundle("Messagebundle", FRLocale);
                    showFrame();
                    taalFrame.dispose();
                }
                );

        taalPanel.add(taalTekstNL);
        taalPanel.add(taalTekstEN);
        taalPanel.add(taalTekstFR);
        taalPanel.add(NLknop);
        taalPanel.add(ENknop);
        taalPanel.add(FRknop);
        taalFrame.add(taalPanel);
        taalFrame.setVisible(true);
        taalFrame.setResizable(false);
    }



    private void alarmPopUp()
    {
        JFrame alarmFrame = new JFrame();
        alarmFrame.setBounds(500, 400, 300, 150);
        JPanel alarmPanel = new JPanel();
        alarmPanel.setBounds(500, 400, 300, 150);
        alarmPanel.setLayout(null);
        JLabel alarmTekst = new JLabel(bundle.getString("tijd om uw medicijnen in te nemen"));
        alarmTekst.setBounds(20, 5, 300, 50);
        JButton close = new JButton(bundle.getString("sluiten"));
        close.setBounds(20, 50, 100, 25);
        close.addActionListener(e ->
        {
            alarmFrame.dispose();
        });
        alarmPanel.add(alarmTekst);
        alarmPanel.add(close);
        alarmFrame.add(alarmPanel);
        alarmFrame.setVisible(true);


    }


    public ZorgApp()
    {

        profielLijst = profielDataReader.getProfileList();
        medicijnLijst = medicijnDataReader.getMedicijnList();

        taalPopUp();

        // aanmaken alarm
        TimerTask alarm = new TimerTask()
        {
            @Override
            public void run()
            {
                String innameTijd = "10:00:00";
                DateFormat datumFormat = new SimpleDateFormat("HH:mm:ss");
                Date huidigeTijd = new Date();
                String stringHuidigeTijd = datumFormat.format(huidigeTijd);
                if (stringHuidigeTijd.equals(innameTijd))
                {
                    alarmPopUp();

                }

            }
        };

        timer.scheduleAtFixedRate(alarm, 0, 1000);


    }

    private void showFrame()
    {
        int widthPane = 700;
        int heightPane = 525;
        int offsetAlignSize = 15;


        String textLabelProfile = bundle.getString("profiel");
        String textLabelMedicijnen = bundle.getString("medicijnen");
        String textLabelGewicht = bundle.getString("gewicht overzicht");


        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setBounds(offsetAlignSize, offsetAlignSize,
                        widthPane - (2 * offsetAlignSize),
                        heightPane - (4 * offsetAlignSize));


        tabbedPane.add(textLabelProfile, getPanelProfile());
        tabbedPane.add(textLabelMedicijnen, getPanelMedicijnen());
        tabbedPane.add(textLabelGewicht, getPanelGewicht());
        add(tabbedPane);


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(widthPane, heightPane);
        setLayout(null);
        setResizable(false);
        setVisible(true);


    }


    private JPanel getPanelProfile()
    {

        JPanel panel = new JPanel();
        JPanel knopPanel = new JPanel();


        //Display data

        for (Profile profile : profielLijst)
        {
            JPanel profielPanel = new JPanel();
            profielPanel.setLayout(new BoxLayout(profielPanel, BoxLayout.Y_AXIS));


            JLabel labelVoornaam = new JLabel(bundle.getString("voornaam") + ": ");
            JTextField textVoornaam = new JTextField(profile.getVoorNaam());

            JLabel labelAchternaam = new JLabel(bundle.getString("achternaam")+ ": ");
            JTextField textAchternaam = new JTextField(profile.getAchterNaam());

            JLabel labelLeeftijd = new JLabel(bundle.getString("leeftijd")+ ": ");
            JTextField textLeeftijd = new JTextField(String.valueOf(profile.getLeeftijd()));

            JLabel labelGewicht = new JLabel(bundle.getString("gewicht")+ ": ");
            JTextField textGewicht = new JTextField(String.valueOf(profile.getGewicht()));

            JLabel labelLengte = new JLabel(bundle.getString("lengte")+ ": ");
            JTextField textLengte = new JTextField(String.valueOf(profile.getLengte()));

            JLabel labelBmi = new JLabel(bundle.getString("bmi") + ": "+ profile.getBmi());



            textVoornaam.setEditable(false);
            textAchternaam.setEditable(false);
            textLeeftijd.setEditable(false);
            textGewicht.setEditable(false);
            textLengte.setEditable(false);



            JButton wijzigKnop = new JButton(bundle.getString("wijzigen"));
            JButton opslaanKnop = new JButton(bundle.getString("opslaan"));
            JButton terugKnop = new JButton(bundle.getString("terug"));



            opslaanKnop.setVisible(false);

            wijzigKnop.addActionListener(e ->
            {
                textVoornaam.setEditable(true);
                textAchternaam.setEditable(true);
                textLeeftijd.setEditable(true);
                textGewicht.setEditable(true);
                textLengte.setEditable(true);
                opslaanKnop.setVisible(true);
                wijzigKnop.setVisible(false);
            });

            opslaanKnop.addActionListener(e ->
            {
                textVoornaam.setEditable(false);
                textAchternaam.setEditable(false);
                textLeeftijd.setEditable(false);
                textGewicht.setEditable(false);
                textLengte.setEditable(false);

                profile.setVoorNaam(textVoornaam.getText());
                profile.setAchterNaam(textAchternaam.getText());
                profile.setLeeftijd(Integer.parseInt(textLeeftijd.getText()));
                profile.setGewicht(Double.parseDouble(textGewicht.getText()));
                profile.setLengte(Double.parseDouble(textLengte.getText()));

                labelBmi.setText(bundle.getString("bmi") + profile.getBmi());

                opslaanKnop.setVisible(false);
                wijzigKnop.setVisible(true);
            });

            terugKnop.addActionListener(e ->
            {
                knopPanel.setVisible(true);
                profielPanel.setVisible(false);
            });

            JButton profielKeuzeKnop = new JButton
                    (profile.getVoorNaam() + " " + profile.getAchterNaam());

            profielKeuzeKnop.addActionListener(e ->
            {
                profielPanel.setVisible(true);
                knopPanel.setVisible(false);
            });


            profielPanel.add(labelVoornaam);
            profielPanel.add(textVoornaam);
            profielPanel.add(labelAchternaam);
            profielPanel.add(textAchternaam);
            profielPanel.add(labelLeeftijd);
            profielPanel.add(textLeeftijd);
            profielPanel.add(labelGewicht);
            profielPanel.add(textGewicht);
            profielPanel.add(labelLengte);
            profielPanel.add(textLengte);
            profielPanel.add(labelBmi);
            profielPanel.add(wijzigKnop);
            profielPanel.add(opslaanKnop);
            profielPanel.add(terugKnop);
            profielPanel.setVisible(false);
            knopPanel.add(profielKeuzeKnop);
            panel.add(profielPanel);
            panel.add(knopPanel);
        }

        return panel;
    }


private JPanel getPanelMedicijnen() {
    JPanel panel = new JPanel();
    JPanel profielpanel = new JPanel();
    panel.setLayout(null);

    // labels met info over medicijnen
    JLabel medicijnNaam = new JLabel(bundle.getString("medicijn naam") + ": ");
    JLabel labelMedicijnNaam = new JLabel("");

    JLabel omschrijving = new JLabel(bundle.getString("omschrijving")+ ": ");
    JLabel labelOmschrijving = new JLabel("");

    JLabel soort = new JLabel(bundle.getString("soort")+ ": ");
    JLabel labelSoort = new JLabel("");

    JLabel dosering = new JLabel(bundle.getString("dosering")+ ": ");
    JLabel labelDosering = new JLabel("");

    JPanel knopPanel = new JPanel();

    JButton terugKnop = new JButton(bundle.getString("terug"));
    terugKnop.addActionListener(e ->
            {
                profielpanel.setVisible(false);
                knopPanel.setVisible(true);
            });

    medicijnNaam.setBounds(35, 20,100,18);
    labelMedicijnNaam.setBounds(150, 20, 400, 18);

    omschrijving.setBounds(35, 50, 100, 18);
    labelOmschrijving.setBounds(150, 50, 400, 18);

    soort.setBounds(35, 80, 100, 18);
    labelSoort.setBounds(150, 80, 400, 18);

    dosering.setBounds(35, 110, 100, 18);
    labelDosering.setBounds(150, 110, 400, 18);

    knopPanel.setBounds(20, 10, 400, 600);
    terugKnop.setBounds(20,230,120,30);

    profielpanel.add(medicijnNaam);
    profielpanel.add(omschrijving);
    profielpanel.add(soort);
    profielpanel.add(dosering);
    profielpanel.add(labelMedicijnNaam);
    profielpanel.add(labelOmschrijving);
    profielpanel.add(labelSoort);
    profielpanel.add(labelDosering);
    profielpanel.add(terugKnop);

    profielpanel.setBounds(20,60,400,600);
    profielpanel.setVisible(false);
    profielpanel.setLayout(null);

    // knoppen voor medicijnen
    int breedte = 20;

    for (Medicijn medicijn : medicijnLijst) {
        JButton medicijnKeuzeKnop = new JButton(medicijn.getMedicijnNaam());
        medicijnKeuzeKnop.addActionListener(e ->
                {
                    labelMedicijnNaam.setText(medicijn.getMedicijnNaam());
                    labelOmschrijving.setText(medicijn.getOmschrijving());
                    labelSoort.setText(medicijn.getSoort());
                    labelDosering.setText(medicijn.getDosering());
                    profielpanel.setVisible(true);
                    knopPanel.setVisible(false);

                }
        );
        medicijnKeuzeKnop.setBounds(breedte, 20, 120, 30);
        knopPanel.add(medicijnKeuzeKnop);
        breedte += 150;
    }

    // aanmaken nieuw medicijn
    JPanel nieuwMedicijnPanel = new JPanel();
    nieuwMedicijnPanel.setLayout(null);
    nieuwMedicijnPanel.setVisible(false);
    nieuwMedicijnPanel.setBounds(20, 170, 500, 300);


    JLabel labelNieuwMedicijnNaam = new JLabel(bundle.getString("medicijn naam") + ":");
    JLabel labelNieuwMedicijnOmschrijving = new JLabel(bundle.getString("omschrijving") + ":");
    JLabel labelNieuwMedicijnSoort = new JLabel(bundle.getString("soort") + ":");
    JLabel labelNieuwMedicijnDosering = new JLabel(bundle.getString("dosering") + ":");

    JTextField textNieuwMedicijnNaam = new JTextField();
    JTextField textNieuwMedicijnOmschrijving = new JTextField();
    JTextField textNieuwMedicijnSoort = new JTextField();
    JTextField textNieuwMedicijnDosering = new JTextField();

    labelNieuwMedicijnNaam.setBounds(20, 20, 100, 18);
    textNieuwMedicijnNaam.setBounds(150, 20, 200, 18);

    labelNieuwMedicijnOmschrijving.setBounds(20, 40, 100, 18);
    textNieuwMedicijnOmschrijving.setBounds(150, 40, 200, 18);

    labelNieuwMedicijnSoort.setBounds(20, 60, 100, 18);
    textNieuwMedicijnSoort.setBounds(150, 60, 200, 18);

    labelNieuwMedicijnDosering.setBounds(20, 80, 100, 18);
    textNieuwMedicijnDosering.setBounds(150, 80, 200, 18);

    JButton medicijnOpslaan = new JButton(bundle.getString("opslaan"));
    medicijnOpslaan.setBounds(20, 110, 120, 30);

    medicijnOpslaan.addActionListener(e ->
    {

        Medicijn nieuwMedicijn = new Medicijn();
        nieuwMedicijn.setMedicijnNaam(textNieuwMedicijnNaam.getText());
        textNieuwMedicijnNaam.setText("");

        nieuwMedicijn.setOmschrijving(textNieuwMedicijnOmschrijving.getText());
        textNieuwMedicijnOmschrijving.setText("");

        nieuwMedicijn.setSoort(textNieuwMedicijnSoort.getText());
        textNieuwMedicijnSoort.setText("");

        nieuwMedicijn.setDosering(textNieuwMedicijnDosering.getText());
        textNieuwMedicijnDosering.setText("");

        medicijnLijst.add(nieuwMedicijn);
        nieuwMedicijnPanel.setVisible(false);
        knopPanel.setVisible(true);
        knopPanel.removeAll();

        int nieuwBreedte = 20;

        for (Medicijn medicijn : medicijnLijst) {
            JButton medicijnKeuzeKnop = new JButton(medicijn.getMedicijnNaam());
            medicijnKeuzeKnop.addActionListener(e1 ->
                    {
                        labelMedicijnNaam.setText(medicijn.getMedicijnNaam());
                        labelOmschrijving.setText(medicijn.getOmschrijving());
                        labelSoort.setText(medicijn.getSoort());
                        labelDosering.setText(medicijn.getDosering());
                        profielpanel.setVisible(true);
                        knopPanel.setVisible(false);
                    }
            );
            medicijnKeuzeKnop.setBounds(nieuwBreedte, 20, 120, 30);
            knopPanel.add(medicijnKeuzeKnop);
            nieuwBreedte += 150;
        }
    });

    nieuwMedicijnPanel.add(labelNieuwMedicijnNaam);
    nieuwMedicijnPanel.add(labelNieuwMedicijnOmschrijving);
    nieuwMedicijnPanel.add(labelNieuwMedicijnSoort);
    nieuwMedicijnPanel.add(labelNieuwMedicijnDosering);
    nieuwMedicijnPanel.add(textNieuwMedicijnNaam);
    nieuwMedicijnPanel.add(textNieuwMedicijnOmschrijving);
    nieuwMedicijnPanel.add(textNieuwMedicijnSoort);
    nieuwMedicijnPanel.add(textNieuwMedicijnDosering);
    nieuwMedicijnPanel.add(medicijnOpslaan);

    JButton toevoegen = new JButton(bundle.getString("toevoegen"));
    toevoegen.setBounds(20, 150, 120, 30);
    toevoegen.addActionListener(e ->
    {
        nieuwMedicijnPanel.setVisible(true);
        profielpanel.setVisible(false);
    });

    // verwijderen van medicijnen
    JPanel verwijderMedicijnPanel = new JPanel();
    verwijderMedicijnPanel.setVisible(false);
    verwijderMedicijnPanel.setBounds(20,170,500,300);

    JButton verwijderen = new JButton(bundle.getString("verwijderen"));
    JLabel verwijdertext = new JLabel(bundle.getString("wat wilt u verwijderen"));
    verwijdertext.setBounds(20,5,120,30);
    verwijderen.setBounds(20,190,120,30);
    verwijderen.addActionListener(e ->
            {
                verwijderMedicijnPanel.removeAll();
                verwijderMedicijnPanel.setVisible(true);
                profielpanel.setVisible(false);

                int breedte1 = 20;
                 for (Medicijn medicijn : medicijnLijst)
                 {
                     JButton medicijnNaam2 = new JButton(medicijn.getMedicijnNaam());
                     medicijnNaam2.addActionListener(e1 ->
                             {
                                 medicijnLijst.remove(medicijn);
                                 int nieuwBreedte = 20;
                                 knopPanel.removeAll();

                                 for (Medicijn medicijn1 : medicijnLijst)
                                 {
                                     JButton medicijnNaam1 = new JButton(medicijn1.getMedicijnNaam());
                                     medicijnNaam1.addActionListener(e2 ->
                                             {
                                                labelMedicijnNaam.setText(medicijn1.getMedicijnNaam());
                                                labelOmschrijving.setText(medicijn1.getOmschrijving());
                                                labelSoort.setText(medicijn1.getSoort());
                                                labelDosering.setText(medicijn1.getDosering());
                                                 profielpanel.setVisible(true);
                                                 knopPanel.setVisible(false);
                                             }
                                             );
                                     medicijnNaam1.setBounds(nieuwBreedte, 20, 120, 30);
                                     knopPanel.add(medicijnNaam1);
                                     nieuwBreedte += 150;
                                 }
                                 verwijderMedicijnPanel.setVisible(false);
                                 profielpanel.setVisible(true);
                             }
                             );
                     medicijnNaam2.setBounds(breedte1, 20, 120, 30);
                     verwijderMedicijnPanel.add(medicijnNaam2);
                     verwijderMedicijnPanel.add(verwijdertext);
                     breedte1 += 150;
                 }

            }
            );


profielpanel.add(toevoegen);
panel.add(nieuwMedicijnPanel);
panel.add(knopPanel);
profielpanel.add(verwijderen);
panel.add(profielpanel);
panel.add(verwijderMedicijnPanel);


return panel;
}

    private JPanel getPanelGewicht()
    {

            JPanel panel = new JPanel();

            panel.add(gewichtGrafiek.LineChartExample(bundle.getString("gewicht grafiek")));


        return panel;
    }
}










