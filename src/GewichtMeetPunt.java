public class GewichtMeetPunt{

    private Double gewicht;
    private String datum;
    private String tijd;


public GewichtMeetPunt() {

    this(null, null, null);
}
public GewichtMeetPunt(Double gewicht,
                       String datum,
                        String tijd){
    this.datum = datum;
    this.tijd = tijd;
    this.gewicht = gewicht;
}

    public String getDatum() {
        return datum;
    }

    public String getTijd() {
        return tijd;
    }

    public Double getGewicht() {
        return gewicht;
    }
}

