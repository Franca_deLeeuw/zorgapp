

public class Tester
{
    private Boolean overallTestResult = true;


    // Run de tester
    public Boolean runTester()
    {
        System.out.println("\nRun tester..");

        int testCase = 1;
        Boolean testResult = testCase1();
        printTestResult(testCase, testResult);

        return overallTestResult;
    }

    private Boolean testCase1()
    {
        // stap voor stap beschreven
        Profile Karin = new Profile("Karin", "Verburgh", 34, 10.0, 1.00);
        // Double object maken om later mee te vergelijken
        String bmi = Karin.getBmi();

        // test uitvoeren en resultaat bewaren

        Boolean testResult = true;

        testResult = setOverallTestResult(testResult);
        return testResult;

    }

    // overallTestResult op false zetten als de testCase niet goed gaat
    private  Boolean setOverallTestResult (Boolean testResult)
    {
        if (!testResult)
        {
            overallTestResult = false;

        }
        return testResult;
    }

    // resultaat uitprinten
    private void printTestResult(int testCase, Boolean testResult)
    {
        System.out.println("\t#Test result @testCase "
                            + String.valueOf(testCase)
                            + ": "
                            + testResult);
    }

}
