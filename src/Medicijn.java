public class Medicijn {

    private String medicijnNaam;
    private String omschrijving;
    private String soort;
    private String dosering;

    public Medicijn() {
        this(null, null, null, null);
    }

    public Medicijn(String medicijnNaam,
                    String omschrijving,
                    String soort,
                    String dosering) {
        this.medicijnNaam = medicijnNaam;
        this.omschrijving = omschrijving;
        this.soort = soort;
        this.dosering = dosering;

    }
    // set variable medicijnNaam
    public void setMedicijnNaam(String medicijnNaam) {

        this.medicijnNaam = medicijnNaam;
    }

    // get variable medicijnNaam
    public String getMedicijnNaam() {
        return medicijnNaam;
    }

    // set variable omschrijving
    public void setOmschrijving(String omschrijving) {

        this.omschrijving = omschrijving;
    }

    // get variable omschrijving
    public String getOmschrijving(){

        return omschrijving;
    }

    //set variable soort
    public void setSoort(String soort) {

        this.soort = soort;
    }

    // get variable soort
    public String getSoort() {

        return soort;
    }

    // set variable dosering
    public void setDosering(String dosering) {

        this.dosering = dosering;
    }

    // get variable dosering

    public String getDosering() {

        return dosering;
    }
}



