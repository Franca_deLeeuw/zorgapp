/*
 * Profile class to store profile data
 *
 * See w3schools.com for Java tutorial
 */

//package adsd.app.zorgapp;


public class Profile {
	// Encapsulate variables
		private String voorNaam;
		private String achterNaam;
		private int leeftijd;
		private double gewicht;
		private double lengte;


	// Constructors with overloading
	// voornaam, achternaam, leeftijd, gewicht, lengte
    public Profile() 
    {
        this(null, null, 0, 0.0, 0.0);
    }
    //
    public Profile(String voorNaam,
    			   String achterNaam)
    {
    	this(voorNaam, achterNaam, 0, 0.0, 0.0);
    }
    //
    public Profile(String voorNaam, 
    			   String achterNaam, 
    			   int leeftijd, 
    			   double gewicht, 
    			   double lengte)
    {
    	this.voorNaam = voorNaam;
    	this.achterNaam = achterNaam;
    	this.leeftijd = leeftijd;
    	this.gewicht = gewicht;
    	this.lengte = lengte;
    }


    // Set variable voorNaam
    public void setVoorNaam(String voorNaam)
    {
    	this.voorNaam = voorNaam;
    }
    //
    // Get variable voorNaam
    public String getVoorNaam()
    {
    	return this.voorNaam;
    }    



    // Set variable achterNaam
    public void setAchterNaam(String achterNaam)
    {
    	this.achterNaam = achterNaam;
    }
    //
    // Get variable achterNaam
    public String getAchterNaam()
    {
    	return this.achterNaam;
    }    


    // Set variable leeftijd
    public void setLeeftijd(int leeftijd)
    {
    	this.leeftijd = leeftijd;
    }
    //
    // Get variable leeftijd
    public int getLeeftijd()
    {
    	return this.leeftijd;
    }    


    // Set variable gewicht
    public void setGewicht(double gewicht)
    {
    	this.gewicht = gewicht;
    }
    //
    // Get variable gewicht
    public double getGewicht()
    {
    	return this.gewicht;
    }    


    // Set variable lengte
    public void setLengte(double lengte)
    {
    	this.lengte = lengte;
    }
    //
    // Get variable lengte
    public double getLengte()
    {
    	return this.lengte;
    }
   

    // Get calculated bmi
    public String getBmi()
    {
    	// return calculated BMI
    	double bmi = gewicht / (lengte * lengte);

    	return String.format("%.2f", bmi);
    }              


}